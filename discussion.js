db.inventory.insertMany([
	{	
		name: "Captain America Shield",
		price: 50000,
		qty: 17,
		company: "Hydra and Co"
	},
	{	
		name: "Mjolnir",
		price: 75000,
		qty: 24,
		company: "Asgard Production"
	},
	{	
		name: "Iron Man Suit",
		price: 25400,
		qty: 25,
		company: "Stark Industries"
	},
	{	
		name: "Eye of Agamotto",
		price: 28000,
		qty: 51,
		company: "Sanctum Company"
	},
	{	
		name: "Iron Spider Suit",
		price: 30000,
		qty: 24,
		company: "Stark Industries"
	}

	])


/* Query Operators
	- allows us to flexible when querying in MongoDB, we can opt to find, update and delete documents based on some conditions instead of specific criterias.

	//Comparison Query Operators
		- $gt and $gte
		// syntax:

			db.collections.find({field: {$gt: value}})
			db.collections.find({field: {$gte: value}})

	$gt - greater than, allows us to find values that are greater than the given value.

	$gte - greater than or equal, allows us to find values that are greater than or equal to the given value

*/

db.inventory.find({price: {$gte: 75000}})



/*
	$lt and $lte
		syntax:
			db. collections.find({field: {$lt: value}})
			db. collections.find({field: {$lte: value}})
*/


db.inventory.find({qty: {$lt: 20}})


/*
	$ne = not equal - returns a document that values are not equal to the given value

	syntax:

		db.collections.find({field: {$ne: value}})

*/


db.inventory.find({qty: {$ne: 10}})


/*
	$in - allows us to find documents that satisfy either of the specified values
		- just like OR
	
	syntax:
		db.collections.find({field: {$in: value}})
*/

db.inventory.find({price: {$in: [25400, 30000]}})
//REMINDER: Although you can express this query using $or operator, choose the $in operator when performing equality checks which are on the same field.


db.inventory.find({price: {$gte: 50000}})

db.inventory.find({qty: {$in: [24, 16]}})



/*update and delete
	2 arguments namely: query criteria, update
	
*/

db.inventory.updateMany({qty: {$lte: 24}}, {$set: {isActive: true}})


db.inventory.updateMany({qty: {$gte: 50}}, {$set: {isActive: false}})




db.inventory.updateMany({price: {$lt: 28000}}, {$set: {qty: 17}})




/*
Logical operators

	$and
	syntax:
		db.collections.find({and: [{criteria1}, {criteria2}]})

	$and - allows us to return documents that satisfies all given conditions
*/



db.inventory.find({$and: [{price: {$gte: 50000}}, {qty: 17}]})



/*


$or - allows us to retur


*/


db.inventory.find({$or:
[
	{qty: {$lt: 24}},
	{isActive: false}

]

})

/*mini activity
1. find a product with qty less than or equal to 24 or a price of less than or equal to 30000.
2. make it an active product*/



db.inventory.updateMany(
	{
		$or: 
		[{qty: {$lte: 24}},
		 {price: {$lte: 30000}}]
             },
             {
                 $set: {isActive: true}
              }
		
	)



/*Evaluation Query Operator

	// $regex
		//syntax
			{field: {regex: /pattern/}}
				- case sensitive query
			
			{field: {$regex: /pattern/, $options: '$optionValue'}}
				- case insensitive query

*/


db.inventory.find({name: {$regex: 'S'}})

db.inventory.find({name: {$regex: 'A', $options: '$i'}})

db.inventory.find({name: {$regex: /[A-C]/}}) // A to C, uppercase



/*mini activity

		in the inventory collection, return document/s that have a name containing a letter "i" and a price greater than 70000
	*/

/*WRONG ANSWER
db.inventory.find(
	{	$and:
		[{price: {$gt: 70000}},
		{name: {$regex: 'i'}}])


db.inventory.find([{name: {$regex: 'i'}},{price: {$gt: 70000}}])

{price: {$gt: 70000}},


*/

db.inventory.find(
{
        $and:
    [
        {
        name: {$regex: 'i', $options: '$i'}
        },
        {
        price: {$gt: 70000}
        }
    ]
}
)


/*field projection
	- allows us to hide/show properties of a return document/s after a query. When dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.

	syntax:
		db.collections.find({criteria}, {field: 1})
			// field: 1 - allows us to include/add specific fields only when retrieving documents.. The value provided is 1 to denote that the field is being included.
			// field: 0 - allows us to exclude the specific field.

*/

db.inventory.find({}, {name: 1, _id: 0}) //only the _id is allowed, mix 0 and 1 are not allowed except for _id. field inclusion (1) and exclusion (0) must not be used in the same time

db.inventory.find({}, {qty: 0}) // excludes quantity data

db.inventory.find({}, {_id: 0, name: 1, price:1 })

db.inventory.find({}, {_id: 0, name: 0, price:0 }) //hides name, price and id



db.inventory.find({company: {$regex: 'A'}}, {name: 1, company:1, _id:0})


/*mini activity
	in the inventory collection, return all documents that have a name containing an uppercase letter 'A' and a price less than or equal to 30000.

	show only the name and price of the product.

	hide the id field.
*/


db.inventory.find(
{
        $and:
    [
        {
        name: {$regex: 'A', }
        },
        {
        price: {$lte: 30000}
        }
    ]
},
{name: 1, price:1, _id:0}
)


